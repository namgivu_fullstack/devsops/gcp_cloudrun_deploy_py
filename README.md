--- info 0th
Deploy a Python service to Cloud Run
ref. https://cloud.google.com/run/docs/quickstarts/build-and-deploy/deploy-python-service

> package it into a container image
> upload the container image to Container Registry
> deploy the container image to Cloud Run

also can do     
> directly in the Cloud Shell Editor

here, we do with gcloud cli 

--- prerequisite
a GCP account 
a gcloud project 
billing enabled 
gcloud cli installed 


--- gcloud cli

== install
ref. https://cloud.google.com/sdk/docs/install
```
sudo apt-get install -y apt-transport-https ca-certificates gnupg

echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] https://packages.cloud.google.com/apt cloud-sdk main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key --keyring /usr/share/keyrings/cloud.google.gpg add -

sudo apt-get update && sudo apt-get install -y google-cloud-cli
```

== init & latest
gcloud init  # will ask to log in if 1sttime, select gcp project if 1sttime
gcloud components update


--- DONE w/ src & Dockerfile ready, run locally          @ r00_install_pkg.sh r00_run_app.sh AND r01_dockerbuildrun.sh
--- DONE w/ src & Dockerfile ready, run w/ gcloud deploy @ rzz_gcloudrundeploy_fr_image.sh
