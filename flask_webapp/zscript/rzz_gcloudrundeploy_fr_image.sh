set -e  # halt if error ON

# gcloud run deploy ref. https://cloud.google.com/sdk/gcloud/reference/run/deploy

SH=`cd $(dirname ${BASH_SOURCE:-$0}) && pwd`

t='gcp_cloudrun_deploy_py_i'

#region build image :t
echo;echo ---
skipbuild=${skipbuild:-1}
if [ ! $skipbuild -eq  1 ]; then
    build_image() {
        t=$t      \
        skiprun=1 \
            exec "$SH/r01_dockerbuildrun.sh"
    }
    build_image  #TODO why this script stop here if we run this line?
fi
#endregion build image :t


echo;echo --- push image
#region push image :t to gcp container registry aka GCR  ref. https://cloud.google.com/container-registry/docs/pushing-and-pulling gg. gcloud cli to push local docker image to gcr
require__gcloud_auth_login_NOTE='call > gcloud auth login AND > gcloud auth configure-docker  ref. https://cloud.google.com/container-registry/docs/advanced-authentication#gcloud-helper'
require__artifact_registry_reroute_NOTE='if you migrate from gcr to artifact, ensure 00 suffice gcp role, 01 route gcr image to artifact  ref https://cloud.google.com/artifact-registry/docs/transition/setup-gcr-repo'

prj='your-gcp-proj'   ; gcr_t="asia.gcr.io/$prj/$t"
#prj='dev-scoot-proj'  ; gcr_t="asia.gcr.io/$prj/$t"

echo;echo;   docker tag $t $gcr_t
echo;echo;   docker push   $gcr_t
echo;echo;   gcloud container images list-tags $gcr_t
#endregion push image :t to gcp container registry aka GCR  ref. https://cloud.google.com/container-registry/docs/pushing-and-pulling gg. gcloud cli to push local docker image to gcr


echo;echo --- deploy fr pushed image
servicename='namgivufullstack-devops-gcpcloudrundeploypy'
gcloud run deploy $servicename   --image $gcr_t   --region asia-southeast2    --allow-unauthenticated


echo;echo --- delete image after deployed
# delete gcr image as cloudrun instance deployed ref. https://cloud.google.com/sdk/gcloud/reference/container/images/delete
gcloud container images delete \
    $gcr_t \
    --force-delete-tags --quiet
