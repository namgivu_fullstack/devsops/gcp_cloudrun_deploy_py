# gcloud run deploy ref. https://cloud.google.com/sdk/gcloud/reference/run/deploy

SH=`cd $(dirname ${BASH_SOURCE:-$0}) && pwd`
GH=`cd $SH/../.. && pwd`
AH="$GH/flask_webapp"

t='gcp_cloudrun_deploy_py_i' ; prj='dev-scoot-proj' ; gcr_t="asia.gcr.io/$prj/$t"

servicename='namgivufullstack-devops-gcpcloudrundeploypy'
gcloud run deploy \
    $servicename \
    --region asia-southeast2    --allow-unauthenticated \
    --source $AH \
    --tag $gcr_t

ERROR_console_output=cat<<EOF
Deploying from source requires an Artifact Registry Docker repository to store built containers. A repository named
[cloud-run-source-deploy] in region [asia-southeast2] will be created.

Do you want to continue (Y/n)?  y

This command is equivalent to running `gcloud builds submit --tag [IMAGE] /home/namgivu/code/l/namgivu_fullstack/devops/gcp_cloudrun_deploy_py/flask_webapp` and `gcloud run deploy namgivufullstack-devops-gcpcloudrundeploypy --image [IMAGE]`

Building using Dockerfile and deploying container to Cloud Run service [namgivufullstack-devops-gcpcloudrundeploypy] in project [dev-scoot-proj] region [asia-southeast2]
X Building and deploying... Updating Service.
  ✓ Creating Container Repository...
  ✓ Uploading sources...
  ✓ Building Container... Logs are available at [https://console.cloud.google.com/cloud-build/builds/c4e70a41-8d03-49e2-9e44-
  9c285809847d?project=109961666521].
  - Creating Revision...
  X Routing traffic... Revision 'namgivufullstack-devops-gcpcloudrundeploypy-00001-cem' is not ready and cannot serve traffic
  . Image 'docker.io/library/gcp_cloudrun_deploy_py_i' does not exist or it is not publicly accessible
  ✓ Setting IAM Policy...
Deployment failed
ERROR: (gcloud.run.deploy) Revision 'namgivufullstack-devops-gcpcloudrundeploypy-00001-cem' is not ready and cannot serve traffic. Image 'docker.io/library/gcp_cloudrun_deploy_py_i' does not exist or it is not publicly accessible
EOF

#TODO hint maybe the image tag should in in form of "asia.gcr.io/$prj/$t" w/ t='gcp_cloudrun_deploy_py_i' prj='dev-scoot-proj'
