set -e  # halt if error ON

SH=`cd $(dirname ${BASH_SOURCE:-$0}) && pwd`
AH=`cd $SH/.. && pwd`

skipinstallpy=${skipinstallpy:-1}
if [ ! $skipinstallpy -eq 1 ]; then
    s='--- install pyenv + update latest';echo;echo $s;     curl --continue-at -  --silent https://pyenv.run | bash; pyenv update
    s='--- install py 3.8 latest';echo;echo $s;             pyenv install --skip-existing 3.8
    s='--- install pip latest';echo;echo $s;                (cd $AH && pyenv local 3.8 && python -m pip install --upgrade pip)
fi

s='--- install pipenv for this py version';echo;echo $s;        (cd $AH && pip install --upgrade pip && pip install pipenv  && pipenv --version  && python -V)
s='--- create .venv';echo;echo $s;                              (cd $AH && PIPENV_VENV_IN_PROJECT=1 pipenv --rm install --clear  && pipenv --venv)
#                   ;echo;echo                                   cd                                        --rm         --clear no cache
