SH=`cd $(dirname ${BASH_SOURCE:-$0}) && pwd`
AH=`cd $SH/.. && pwd`

cd $AH
    #                         gunicorn   --workers 1 --threads 8 Run the web service on container startup. Here we use the gunicorn webserver, with one worker process and 8 threads. For environments with `multiple CPU cores`, increase the `number of workers to be equal` to the cores available.
    #                         gunicorn                            --timeout 0 Timeout is set to 0 to disable the timeouts of the workers to allow Cloud Run to handle instance scaling.
    PYTHONPATH=$AH pipenv run gunicorn   --workers 1 --threads 8  --timeout 0   --bind :${PORT:-8000}   src.main:app
