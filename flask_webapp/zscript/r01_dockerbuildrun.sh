SH=`cd $(dirname ${BASH_SOURCE:-$0}) && pwd`
GH=`cd $SH/../.. && pwd`
AH="$GH/flask_webapp"
cd $AH
    t=${t:-gcp_cloudrun_deploy_py_i}

    skipbuild=${skipbuild:-0}
    if [ ! $skipbuild -eq 1 ]; then
        docker build   -f "$AH/Dockerfile"   -t $t   --no-cache   $AH
    fi

    skiprun=${skiprun:-0}
    if [ ! $skiprun -eq 1 ]; then
        [ -z $PORT ] && (echo 'Envvar PORT is required' && kill $$)
        docker run -d    -p ${PORT}:8000 $t
    fi
